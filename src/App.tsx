import React from 'react';
import axios from 'axios';

import { Header, MovieItem } from './components/index';

import { IMovie } from './interfaces';

import './scss/app.scss';

const FEATURED_API =
  'https://api.themoviedb.org/3/discover/movie?api_key=e2d81687832488ee101907e0df3d9f86';
const SEARCH_API =
  'https://api.themoviedb.org/3/search/movie?api_key=e2d81687832488ee101907e0df3d9f86&query=';

const App: React.FC = () => {
  const [movies, setMovies] = React.useState<IMovie[]>([]);
  const [searchTerm, setSearchTerm] = React.useState<string>('');

  const getMovies = (API: string) => {
    axios.get(API).then(({ data }) => setMovies(data.results));
  };

  const onHandleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(e.target.value);
  };

  React.useEffect(() => {
    if (searchTerm) {
      getMovies(SEARCH_API + searchTerm);
    } else {
      getMovies(FEATURED_API);
    }
  }, [searchTerm]);

  return (
    <div className='app'>
      <Header onSearch={onHandleChange} searchValue={searchTerm} />
      <main className='movies'>
        <div className='container'>
          <div className='movies__inner'>
            {movies &&
              movies.map((movie) => (
                <MovieItem key={movie.id} title={movie.title} poster={movie.poster_path} />
              ))}
          </div>
        </div>
      </main>
    </div>
  );
};

export default App;
