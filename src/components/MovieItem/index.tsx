import React from 'react';

interface MovieItemProps {
  title: string;
  poster: string;
}

const MovieItem: React.FC<MovieItemProps> = ({ title, poster }) => {
  return (
    <div className='movies__item'>
      <div className='movies__box'>
        <img
          src={
            !poster
              ? 'https://kinomozg.com/images/noposter.png'
              : `https://image.tmdb.org/t/p/original/${poster}`
          }
          alt='movie'
        />
      </div>
      <span className='movies__name'>{title}</span>
    </div>
  );
};

export default MovieItem;
