import Header from './Header';
import MovieItem from './MovieItem';

export { Header, MovieItem };
