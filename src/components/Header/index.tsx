import React from 'react';

interface HeaderProps {
  onSearch: (e: React.ChangeEvent<HTMLInputElement>) => void;
  searchValue: string;
}

const Header: React.FC<HeaderProps> = ({ onSearch, searchValue }) => {
  return (
    <header className='header'>
      <div className='container'>
        <div className='header__inner'>
          <a href='/#' className='logo'>
            movie<span>s</span>
          </a>
          <label className='header__search'>
            <input
              type='input'
              placeholder='Найти фильм...'
              onChange={onSearch}
              value={searchValue}
            />
          </label>
        </div>
      </div>
    </header>
  );
};

export default Header;
